# Pallas Trace Collection

The Pallas trace collection contains traces of HPC applications to the research community for analysis.


## Traces

### MPI traces
- [NAS Parallel Benchmarks](traces/mpi/npb.md)

### OpenMP traces
- [NAS Parallel Benchmarks](traces/openmp/npb.md)

### MPI+OpenMP traces
- [NAS Parallel Benchmarks](traces/mpi_openmp/npb.md)

## Hardware setting
The traces were collected by running applications on several machines or clusters, including:

- [Sandor](hardware/sandor.md)
- [Bran](hardware/bran.md)


## Software

- Tracing tool: EZTrace.
- Trace format: Pallas.
- Visualizing the traces: ViTE



## Contact

[François Trahay](mailto:francois.trahay@telecom-sudparis.eu)

[Inria Benagil group](https://team.inria.fr/benagil/)  at [Télécom SudParis](https://www.telecom-sudparis.eu/) / [Institut Polytechnique de Paris](https://www.ip-paris.fr/)
